<?php

namespace WarpedDimension\GazpachoSoup;

use Exception;
use InvalidArgumentException;
use JetBrains\PhpStorm\ExpectedValues;
use ReflectionAttribute;
use ReflectionClass;
use ReflectionException;
use ReflectionMethod;
use ReflectionNamedType;
use ReflectionParameter;
use ReflectionType;
use ReflectionUnionType;
use Throwable;
use TypeError;
use WarpedDimension\GazpachoSoup\Authentication\Authenticated;
use WarpedDimension\GazpachoSoup\Exceptions\BadParameterException;
use WarpedDimension\GazpachoSoup\Exceptions\MissingRequiredParameterException;
use WarpedDimension\GazpachoSoup\Extractors\IParameterExtractor;
use WarpedDimension\GazpachoSoup\Extractors\MethodParameter;
use WarpedDimension\GazpachoSoup\ModelParser\HandlerOverrideRegistry;
use WarpedDimension\GazpachoSoup\ModelParser\IRequestModel;
use WarpedDimension\GazpachoSoup\Results\IResult;
use WarpedDimension\GazpachoSoup\Transformers\IParameterTransformer;

/**
 * Wraps a route method in order to extract information from the attributes.
 *
 * @package WarpedDimension\GazpachoSoup
 * @author  Ned Hyett <business@warped-dimension.com>
 */
class RouteWrapper
{

    /**
     * @var ReflectionMethod the method to invoke
     */
    private ReflectionMethod $method;

    /**
     * @var ControllerBase the instance of the controller that contains this route
     */
    private ControllerBase $controller;

    /**
     * RouteWrapper constructor.
     *
     * @param ControllerBase   $controller
     * @param ReflectionMethod $method
     */
    public function __construct( ControllerBase $controller, ReflectionMethod $method )
    {
        $this->method = $method;
        $this->controller = $controller;
    }

    public function getMethod(): ReflectionMethod
    {
        return $this->method;
    }

    public function getController(): ControllerBase
    {
        return $this->controller;
    }

    /**
     * Gets any {@see IParameterExtractor} from the {@see ReflectionParameter} of a route that is available.
     *
     * @param ReflectionParameter $parameter
     *
     * @return IParameterExtractor|null
     */
    private static function getExtractorFromParameter( ReflectionParameter $parameter ): ?IParameterExtractor
    {
        $attrs = $parameter->getAttributes();
        foreach ( $attrs as $attribute )
        {
            if ( is_a($attribute->getName(), IParameterExtractor::class, true) )
            {
                /** @var IParameterExtractor $extractor */
                /** @noinspection PhpUnnecessaryLocalVariableInspection */
                $extractor = $attribute->newInstance();
                return $extractor;
            }
        }
        return null;
    }

    /**
     * Get the authenticated attribute from the method.
     *
     * @return Authenticated|null
     * @throws Exception
     */
    public function getAuthenticatedAttribute(): ?Authenticated
    {
        $attrs = $this->method->getAttributes(Authenticated::class, ReflectionAttribute::IS_INSTANCEOF);
        if ( count($attrs) === 0 )
            return null;
        $inst = $attrs[0]->newInstance();
        if ( $inst instanceof Authenticated )
            return $inst;
        // @codeCoverageIgnoreStart
        throw new Exception(sprintf('Metadata attribute was somehow not of correct type?!? This should never happen. (expected %s, got %s)', Authenticated::class, gettype($inst)));
        // @codeCoverageIgnoreEnd
    }

    /**
     * Get the route attribute from the method.
     *
     * @return Route|null
     * @throws Exception
     */
    public function getRouteAttribute(): ?Route
    {
        $attrs = $this->method->getAttributes(Route::class, ReflectionAttribute::IS_INSTANCEOF);
        if ( count($attrs) === 0 )
            return null;
        $inst = $attrs[0]->newInstance();
        if ( $inst instanceof Route )
            return $inst;
        // @codeCoverageIgnoreStart
        throw new Exception(sprintf('Metadata attribute was somehow not of correct type?!? This should never happen. (expected %s, got %s)', Route::class, gettype($inst)));
        // @codeCoverageIgnoreEnd
    }

    /**
     * Check if the route requires authentication.
     *
     * @throws Exception
     */
    public function isAuthenticated(): bool
    {
        return $this->getAuthenticatedAttribute() !== null;
    }

    /**
     * Get any parameters from the function that have any {@see IParameterExtractor} attribute.
     *
     * @param class-string|null $className
     *
     * @return array[]
     * @throws ReflectionException
     */
    public function getParametersWithExtractors( ?string $className = null ): array
    {
        $params = [];
        foreach ( $this->method->getParameters() as $v )
        {
            $attrs = $v->getAttributes();
            foreach ( $attrs as $attribute )
            {
                if ( is_a($attribute->getName(), IParameterExtractor::class, true) )
                {
                    if ( $className !== null && !is_a($className, $attribute->getName(), true) )
                        continue;
                    /** @var IParameterExtractor $inst */
                    $inst = $attribute->newInstance();
                    $params[] = [
                        'parameter' => $v,
                        'attr' => $inst,
                        'name' => $v->getName(),
                        'type' => $v->getType(),
                        'optional' => $v->isOptional(),
                        'default' => $v->isOptional() ? $v->getDefaultValue() : null
                    ];
                }
            }
        }
        return $params;
    }

    /**
     * Get any parameters that don't have an {@see IParameterExtractor} attribute.
     *
     * @return array[]
     * @throws ReflectionException
     */
    public function getPathParameters(): array
    {
        $params = [];
        foreach ( $this->method->getParameters() as $v )
        {
            $attrs = $v->getAttributes();
            if ( count($attrs) > 0 )
            {
                foreach ( $attrs as $attr )
                {
                    if ( is_a($attr->getName(), IParameterExtractor::class, true) )
                        continue 2;
                    //Skip the "Method Parameter" too.
                    if ( is_a($attr->getName(), MethodParameter::class, true) )
                        continue 2;
                }
            }
            $params[ $v->getName() ] = [
                'parameter' => $v,
                'name' => $v->getName(),
                'type' => $v->getType(),
                'optional' => $v->isOptional(),
                'default' => $v->isOptional() ? $v->getDefaultValue() : null
            ];
        }
        return $params;
    }

    /**
     * Create an artificial index array for path parameters and skipping any that have a {@see IParameterExtractor} attribute.
     *
     * So looking for index of "test3" from "test1, (getattr)test2, test3" would return 1 because test2 is ignored.
     *
     * @param ReflectionParameter $parameter
     *
     * @return false|int
     * @throws ReflectionException
     */
    public function getPathParameterIndex( ReflectionParameter $parameter ): false|int
    {
        $pathParams = $this->getPathParameters();
        usort($pathParams, function( array $a, array $b ) {
            /** @var ReflectionParameter $aParam */
            $aParam = $a['parameter'];
            /** @var ReflectionParameter $bParam */
            $bParam = $b['parameter'];
            return ($aParam->getPosition() < $bParam->getPosition()) ? -1 : 1;
        });
        for ( $i = 0; $i < count($pathParams); $i++ )
        {
            /** @var ReflectionParameter $param */
            $param = $pathParams[ $i ]['parameter'];
            if ( $param->getName() === $parameter->getName() )
                return $i;
        }
        return false;
    }

    /**
     * Compute the actual value of the parameter, resolving the {@see IParameterExtractor} and the correct {@see IRequestModel} values.
     *
     * TODO: move resolution of IRequestModel here
     *
     * @param array               $pathParameters
     * @param ReflectionParameter $parameter
     *
     * @return mixed
     * @throws MissingRequiredParameterException
     * @throws ReflectionException
     */
    private function getValueFromParameter( array $pathParameters, ReflectionParameter $parameter ): mixed
    {
        $extractor = self::getExtractorFromParameter($parameter);
        if ( $extractor !== null )
        {
            if ( !$extractor->isSet($parameter->getName()) )
            {
                if ( !$parameter->isOptional() )
                    throw new MissingRequiredParameterException(null, $parameter->getName(), strtolower($extractor->getLocationName()));
                return null;
            }
            return $extractor->getParameterValue($parameter->getName());
        }
        else
        {
            //It's a path parameter
            if ( !isset($pathParameters[ $parameter->getName() ]) && !isset($pathParameters[ $this->getPathParameterIndex($parameter) ]) )
            {
                if ( !$parameter->isOptional() )
                    throw new MissingRequiredParameterException(null, $parameter->getName(), 'path');
                return null;
            }
            if ( isset($pathParameters[ $parameter->getName() ]) )
            {
                return $pathParameters[ $parameter->getName() ];
            }
            else if ( isset($pathParameters[ $pathParameterIndex = $this->getPathParameterIndex($parameter) ]) )
            {
                return $pathParameters[ $pathParameterIndex ];
            }
        }
        return null;
    }

    /**
     * Check if a reflection type could possibly accept any type of class
     *
     * @param ReflectionType $reflectionType
     * @param class-string   $className
     *
     * @return bool|string false if no, string = name of the class type that was matched
     */
    private function reflectionTypeIsA( ReflectionType $reflectionType, string $className ): bool|string
    {
        if ( $reflectionType instanceof ReflectionNamedType )
        {
            return is_a($reflectionType->getName(), $className, true) ? $reflectionType->getName() : false;
        }
        else if ( $reflectionType instanceof ReflectionUnionType )
        {
            foreach ( $reflectionType->getTypes() as $type )
            {
                if ( is_a($type->getName(), $className, true) )
                    return $type->getName();
            }
            return false;
        }
        else
            throw new InvalidArgumentException();
    }

    /**
     * TODO: move to util class
     *
     * @param ReflectionType $reflectionType
     *
     * @return string
     */
    private function getReflectionTypeString( ReflectionType $reflectionType ): string
    {
        if ( $reflectionType instanceof ReflectionNamedType )
            return $reflectionType->getName();
        else if ( $reflectionType instanceof ReflectionUnionType )
        {
            $str = '';
            foreach ( $reflectionType->getTypes() as $type )
            {
                $str .= $type->getName() . '|';
            }
            return substr($str, 0, strlen($str) - 1);
        }
        else
            throw new InvalidArgumentException();
    }

    /**
     * Run the parameter through the parameter transformers to preprocess the parameter value.
     *
     * @param ReflectionParameter $parameter
     * @param mixed               $value
     * @param int                 $mode
     *
     * @return mixed
     */
    private function runParameterTransformers( ReflectionParameter $parameter, mixed $value, #[ExpectedValues(valuesFromClass: IParameterTransformer::class)] int $mode ): mixed
    {
        /** @var IParameterTransformer[] $transformers */
        $transformers = array_map(
            function( ReflectionAttribute $attr ) {
                return $attr->newInstance();
            },
            array_filter($parameter->getAttributes(), function( ReflectionAttribute $attr ): bool {
                return is_a($attr->getName(), IParameterTransformer::class, true);
            })
        );
        foreach ( $transformers as $transformer )
        {
            $value = match ($mode)
            {
                IParameterTransformer::MODE_PRE => $transformer->transformPre($parameter, $value),
                IParameterTransformer::MODE_POST => $transformer->transformPost($parameter, $value),
            };
        }
        return $value;
    }

    /**
     * Invoke this route.
     *
     * @param array $pathParameters the parameters extracted from the URL
     * @param int   $requestMethod  the requesting method from {@see HttpVerbs} TODO: pass this to the route
     *
     * @return IResult A response to write to the output.
     * @throws Throwable Route can throw anything to send an error to the client.
     */
    public function invoke( array $pathParameters, #[ExpectedValues(valuesFromClass: HttpVerbs::class)] int $requestMethod ): IResult
    {
        $parametersToInvokeWith = [];
        foreach ( $this->method->getParameters() as $parameter )
        {
            //Check if this parameter wants the method injected
            if ( count($parameter->getAttributes(MethodParameter::class)) > 0 )
            {
                $parametersToInvokeWith[ $parameter->getName() ] = $requestMethod;
                continue;
            }

            $value = $this->getValueFromParameter($pathParameters, $parameter);
            $value = $this->runParameterTransformers($parameter, $value, IParameterTransformer::MODE_PRE);
            if ( is_null($value) )
            {
                if ( !$parameter->allowsNull() )
                    throw new BadParameterException(null, $parameter->getName(), 'null', $this->getReflectionTypeString($parameter->getType()));
                continue;
            }

            if ( $parameter->getType() !== null && ($typeName = $this->reflectionTypeIsA($parameter->getType(), IRequestModel::class)) !== false )
            {
                $rc = new ReflectionClass($typeName);
                $value = $rc->getMethod('createFromRequestValue')->invoke(null, $value);
            }

            if ( $parameter->getType() !== null )
            {
                $paramType = $parameter->getType();
                if ( $paramType instanceof ReflectionNamedType )
                {
                    /** @var ReflectionNamedType $paramTypeNamed */
                    $paramTypeNamed = $paramType;
                    if ( HandlerOverrideRegistry::hasHandler($paramTypeNamed->getName()) )
                        $value = HandlerOverrideRegistry::attemptConversion($paramTypeNamed->getName(), $parameter, $value);
                }
            }

            $value = $this->runParameterTransformers($parameter, $value, IParameterTransformer::MODE_POST);

            $parametersToInvokeWith[ $parameter->getName() ] = $value;
        }
        try
        {
            return $this->method->invoke($this->controller, ...$parametersToInvokeWith);
        }
        catch ( TypeError $ex )
        {
            //We can only blame a bad input parameter if the invoke is the second item in the trace.
            $trace = $ex->getTrace();
            if ( count($trace) >= 2 )
            {
                $traceLine = $trace[1];
                if ( !isset($traceLine['file']) || !isset($traceLine['function']) || !isset($traceLine['class']) )
                    throw $ex;
                if ( $traceLine['file'] !== __FILE__ || $traceLine['function'] !== 'invoke' || $traceLine['class'] !== ReflectionMethod::class )
                    throw $ex;
            }

            //Wrap the TypeError with a custom exception to hide the stack trace from users in case this error makes it to production.
            if ( preg_match('/Argument #[\d]* \(\$(.*)\) must be of type (.*), (.*) given/', $ex->getMessage(), $matches) === 1 )
                throw new BadParameterException($ex, $matches[1], $matches[3], $matches[2]);
            else
                throw $ex;
        }
    }

}