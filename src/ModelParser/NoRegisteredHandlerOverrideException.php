<?php

namespace WarpedDimension\GazpachoSoup\ModelParser;

use WarpedDimension\GazpachoSoup\Exceptions\GazpachoSoupException;

class NoRegisteredHandlerOverrideException extends GazpachoSoupException
{

    const CODE = 105;
    const MESSAGE = "Handler for %s was not found.";

    /**
     * @param class-string $class
     */
    public function __construct( string $class )
    {
        parent::__construct(null, $class);
    }

}