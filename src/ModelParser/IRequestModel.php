<?php

namespace WarpedDimension\GazpachoSoup\ModelParser;

/**
 * This class can be instantiated from data provided by the router.
 *
 * @package WarpedDimension\GazpachoSoup
 * @author  Ned Hyett <business@warped-dimension.com>
 */
interface IRequestModel
{

    /**
     * Create an instance of this model from the provided router input.
     *
     * @param mixed $input
     *
     * @return object
     */
    static function createFromRequestValue( mixed $input ): object;

}