<?php

namespace WarpedDimension\GazpachoSoup\ModelParser;

use Attribute;

#[Attribute(Attribute::TARGET_METHOD)]
class HandlerOverride
{

    /**
     * @var class-string
     */
    private string $className;

    /**
     * @param class-string $className
     */
    public function __construct( string $className )
    {
        $this->className = $className;
    }

    /**
     * @return string
     */
    public function getTargetClassName(): string
    {
        return $this->className;
    }

}