<?php

namespace WarpedDimension\GazpachoSoup\Finders;

/**
 * Defines a finder for controller classes.
 *
 * @package WarpedDimension\GazpachoSoup\Finders
 * @author  Ned Hyett <business@warped-dimension.com>
 */
interface IControllerFinder
{

    /**
     * Get a list of all possible controller classes in namespace format.
     *
     * @return array
     */
    public function getControllerNamespaces(): array;
}