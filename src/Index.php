<?php

namespace WarpedDimension\GazpachoSoup;

use Attribute;

/**
 * Shorthand for an index route that matches either the controller route or controller route + /index.
 */
#[Attribute(Attribute::TARGET_CLASS | Attribute::TARGET_METHOD)]
class Index extends Route
{
    public function __construct( int $method = HttpVerbs::VERB_GET )
    {
        parent::__construct('((\/index)?)', $method);
    }

}