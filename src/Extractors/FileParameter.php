<?php

namespace WarpedDimension\GazpachoSoup\Extractors;

use Attribute;
use WarpedDimension\GazpachoSoup\Exceptions\FileNotAllowedException;

/**
 * Pass an entry in the {@see $_FILES} array to the route in the specified parameter.
 *
 * @package WarpedDimension\GazpachoSoup\ParameterHandlers
 * @author  Ned Hyett <business@warped-dimension.com>
 */
#[Attribute(Attribute::TARGET_PARAMETER)]
class FileParameter implements IParameterExtractor
{

    /**
     * @var string|null The field to read from the {@see $_FILES} array. If not null, overrides reading the parameter name instead.
     */
    public ?string $fileField = null;

    /**
     * @var array|null A list of allowed mime types (or null to allow any)
     */
    public ?array $allowedMimeTypes = null;

    /**
     * FileParameter constructor.
     *
     * @param string|null $fileField        an override for the file field, using this instead of the parameter name
     * @param array|null  $allowedMimeTypes an array of allowed MIME type regexes (or null to allow anything)
     */
    public function __construct( ?string $fileField = null, ?array $allowedMimeTypes = null )
    {
        $this->fileField = $fileField;
        $this->allowedMimeTypes = $allowedMimeTypes;
    }

    /**
     * Get the mime type of the file.
     *
     * @param string $funcParamName the name of the parameter in the route.
     *
     * @return string|null the mime type
     */
    function getMimeType( string $funcParamName ): ?string
    {
        $paramName = $this->fileField ?? $funcParamName;
        return $_FILES[ $paramName ]['type'];
    }

    /**
     * Check if the file mime type is allowed into this route.
     *
     * @param string $funcParamName the name of the parameter in the route.
     *
     * @return bool
     */
    function isValidMimeType( string $funcParamName ): bool
    {
        if ( $this->allowedMimeTypes === null )
            return true;
        $fileMime = $this->getMimeType($funcParamName);
        foreach ( $this->allowedMimeTypes as $mimeType )
        {
            if ( preg_match_all(sprintf('/^%s$/', str_replace('/', '\/', $mimeType)), $fileMime) )
                return true;
        }
        return false;
    }

    /**
     * Get a string of all the mime types that are allowed for the bad mime exception.
     *
     * @return string
     */
    function getMimeTypeListString(): string
    {
        if ( $this->allowedMimeTypes === null )
            return "";
        $str = "";
        foreach ( $this->allowedMimeTypes as $mimeType )
        {
            $str .= $mimeType . ', ';
        }
        return substr($str, 0, strlen($str) - 2);
    }

    /**
     * @inheritdoc
     * @throws FileNotAllowedException
     */
    function getParameterValue( string $funcParamName ): mixed
    {
        if ( !$this->isSet($funcParamName) )
            return null;
        if ( !$this->isValidMimeType($funcParamName) )
            throw new FileNotAllowedException(null, $this->getMimeType($funcParamName), $this->getMimeTypeListString());
        return $_FILES[ $this->fileField ?? $funcParamName ];
    }

    /**
     * @inheritdoc
     */
    function isSet( string $funcParamName ): bool
    {
        return array_key_exists($this->fileField ?? $funcParamName, $_FILES);
    }

    /**
     * @inheritdoc
     */
    function getLocationName(): string
    {
        return 'File';
    }

}