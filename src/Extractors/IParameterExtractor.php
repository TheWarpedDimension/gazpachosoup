<?php

namespace WarpedDimension\GazpachoSoup\Extractors;

/**
 * Class of annotation that can pull data from an external resource for a route parameter.
 *
 * @package WarpedDimension\GazpachoSoup
 * @author  Ned Hyett <business@warped-dimension.com>
 */
interface IParameterExtractor
{

    /**
     * Get the parameter from the location.
     *
     * @param string $funcParamName the parameter name from the route
     *
     * @return mixed
     */
    function getParameterValue( string $funcParamName ): mixed;

    /**
     * Check if the parameter is available for getting.
     *
     * @param string $funcParamName the parameter name from the route
     *
     * @return bool
     */
    function isSet( string $funcParamName ): bool;

    /**
     * String that identifies this "location" in the error output.
     *
     * @return string
     */
    function getLocationName(): string;

}