<?php

namespace WarpedDimension\GazpachoSoup\Extractors;

use Attribute;

/**
 * Pass an entry in the {@see $_GET} array to the route in the specified parameter.
 *
 * @package WarpedDimension\GazpachoSoup\ParameterHandlers
 * @author  Ned Hyett <business@warped-dimension.com>
 */
#[Attribute(Attribute::TARGET_PARAMETER)]
class QueryParameter implements IParameterExtractor
{
    /**
     * @var string|null The field to read from the {@see $_GET} array. If not null, overrides reading the parameter name instead.
     */
    public ?string $getField = null;

    /**
     * QueryParameter constructor.
     *
     * @param string|null $getField an override for the get field, using this instead of the parameter name
     */
    public function __construct( ?string $getField = null )
    {
        $this->getField = $getField;
    }

    /**
     * @inheritdoc
     */
    function getParameterValue( string $funcParamName ): mixed
    {
        if ( isset($_GET[ $this->getField ?? $funcParamName ]) )
            return $_GET[ $this->getField ?? $funcParamName ];
        return null;
    }

    /**
     * @inheritdoc
     */
    function isSet( string $funcParamName ): bool
    {
        return array_key_exists($this->getField ?? $funcParamName, $_GET);
    }

    /**
     * @inheritdoc
     */
    function getLocationName(): string
    {
        return 'Query String';
    }
}