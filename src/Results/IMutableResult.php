<?php

namespace WarpedDimension\GazpachoSoup\Results;

/**
 * Interface for mutating an {@see IResult}. All results should extend this instead of IResult.
 *
 * @package WarpedDimension\GazpachoSoup\Results
 * @author  Ned Hyett <business@warped-dimension.com>
 * @created 10/08/2021
 */
interface IMutableResult extends IResult
{

    function setHttpCode( int $code ): static;

    function setCode( int $code ): static;

    function setHeader( string $name, mixed $value ): static;

    function removeHeader( string $name ): static;

    function clearHeaders(): static;

    function setBody( mixed $body ): static;

}