<?php

namespace WarpedDimension\GazpachoSoup;

use InvalidArgumentException;
use JetBrains\PhpStorm\ExpectedValues;

/**
 * List of HTTP Verbs
 *
 * @package WarpedDimension\GazpachoSoup
 * @author  Ned Hyett <business@warped-dimension.com>
 */
abstract class HttpVerbs
{
    /**
     * Get a resource. Request should not produce side-effects.
     */
    const VERB_GET = 0x1;

    /**
     * Get the headers for a resource. Request should not produce side-effects.
     */
    const VERB_HEAD = 0x2;

    /**
     * Upload data to the server.
     */
    const VERB_POST = 0x4;

    /**
     * Store data on the server.
     */
    const VERB_PUT = 0x8;

    /**
     * Delete data from the server.
     */
    const VERB_DELETE = 0x10;

    /**
     * Echo the request
     */
    const VERB_TRACE = 0x20;

    /**
     * Get the options that the server supports for this url
     */
    const VERB_OPTIONS = 0x40;

    /**
     * Convert to TCP/IP tunnel
     */
    const VERB_CONNECT = 0x80;

    /**
     * Apply partial modification to the resource
     */
    const VERB_PATCH = 0x100;

    /**
     * Convert a string to an HTTP verb flag.
     *
     * @param string $str
     *
     * @return int
     */
    #[ExpectedValues(valuesFromClass: HttpVerbs::class)]
    public static function getFromString( string $str ): int
    {
        return match (strtolower($str))
        {
            "get" => self::VERB_GET,
            "head" => self::VERB_HEAD,
            "post" => self::VERB_POST,
            "put" => self::VERB_PUT,
            "delete" => self::VERB_DELETE,
            "trace" => self::VERB_TRACE,
            "options" => self::VERB_OPTIONS,
            "connect" => self::VERB_CONNECT,
            "patch" => self::VERB_PATCH,
            default => throw new InvalidArgumentException(),
        };
    }

    /**
     * Get the current HTTP verb as a verb flag.
     *
     * @return int
     */
    #[ExpectedValues(valuesFromClass: HttpVerbs::class)]
    public static function getCurrent(): int
    {
        return self::getFromString($_SERVER['REQUEST_METHOD']);
    }
}