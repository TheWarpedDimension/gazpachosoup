<?php

namespace WarpedDimension\GazpachoSoup;

use ReflectionClass;
use ReflectionException;
use Throwable;
use WarpedDimension\GazpachoSoup\Authentication\Authenticated;
use WarpedDimension\GazpachoSoup\Authentication\IAuthenticationHandler;
use WarpedDimension\GazpachoSoup\Exceptions\ClientForbiddenException;
use WarpedDimension\GazpachoSoup\Exceptions\IRouterException;
use WarpedDimension\GazpachoSoup\Exceptions\MethodNotAllowedException;
use WarpedDimension\GazpachoSoup\Exceptions\PageNotFoundException;
use WarpedDimension\GazpachoSoup\Finders\IControllerFinder;
use WarpedDimension\GazpachoSoup\Results\IResult;
use WarpedDimension\GazpachoSoup\Results\IResultTransformer;
use WarpedDimension\GazpachoSoup\Results\JsonAPIResult;

/**
 * Routes requests.
 *
 * @package WarpedDimension\GazpachoSoup
 * @author  Ned Hyett <business@warped-dimension.com>
 */
class Router
{

    /**
     * @var ControllerBase[] a cache of controllers that have been loaded
     */
    private array $controllers = [];

    /**
     * @var bool Has the router searched for controllers yet?
     */
    private bool $searchedForControllers = false;

    /**
     * @var IControllerFinder[] a controller finder that locates controllers.
     */
    private array $controllerFinders;

    /**
     * @var IAuthenticationHandler|null a handler for authentication requests from the {@see Authenticated} attribute. TODO: allow multiple of these.
     */
    private ?IAuthenticationHandler $authenticationHandler = null;

    /**
     * @var IErrorHandler|null a handler for errors raised during the routing process. TODO: allow multiple of these.
     */
    private ?IErrorHandler $errorHandler = null;

    /**
     * @var IResultTransformer[] a list of all result transformers
     */
    private array $resultTransformers = [];

    /**
     * Router constructor.
     *
     * @param IControllerFinder|array $controllerFinder
     */
    public function __construct( IControllerFinder|array $controllerFinder )
    {
        if ( is_array($controllerFinder) )
        {
            $this->controllerFinders = $controllerFinder;
        }
        else
        {
            $this->controllerFinders = [ $controllerFinder ];
        }
    }

    /**
     * Set the authentication handler
     *
     * @param ?IAuthenticationHandler $authenticationHandler
     *
     * @return static
     */
    public function setAuthenticationHandler( ?IAuthenticationHandler $authenticationHandler ): static
    {
        $this->authenticationHandler = $authenticationHandler;
        return $this;
    }

    /**
     * Force the registered authentication handler to give you a response outside of routing.
     *
     * @param ControllerBase     $controller
     * @param Authenticated|null $context
     *
     * @return bool
     */
    public function checkAuthentication( ControllerBase $controller, ?Authenticated $context = null ): bool
    {
        try
        {
            return $this->authenticationHandler?->checkAuthentication($controller, null, $context) ?? false;
        }
        catch ( Throwable $ex )
        {
            error_log($ex->getMessage());
            error_log($ex->getTraceAsString());
            return false;
        }
    }

    /**
     * Set the handler for errors.
     *
     * @param IErrorHandler $errorHandler
     *
     * @return $this
     */
    public function setErrorHandler( IErrorHandler $errorHandler ): static
    {
        $this->errorHandler = $errorHandler;
        return $this;
    }

    /**
     * Get all controllers registered to the router.
     *
     * If no controllers were registered to the router, search for some.
     *
     * @throws ReflectionException
     */
    public function getControllers(): array
    {
        if ( $this->searchedForControllers )
            return $this->controllers;
        $this->searchedForControllers = true;

        $paths = [];

        foreach ( $this->controllerFinders as $finder )
        {
            $paths = array_merge($paths, $finder->getControllerNamespaces());
        }

        foreach ( $paths as $path )
        {
            $rc = new ReflectionClass($path);
            if ( !($path)::enabled() )
                continue;
            /** @var ControllerBase $controller */
            $controller = $rc->newInstance();
            $this->registerController($controller);
        }

        return $this->controllers;
    }

    /**
     * Register a new controller to the router.
     *
     * TODO: add duplicate detection
     *
     * @param ControllerBase $newController
     *
     * @return static
     */
    public function registerController( ControllerBase $newController ): static
    {
//        if ( count(array_filter($this->controllers, function( $oldController ) use ( $newController ) {
//            return $newController instanceof $oldController;
//        })) )
//            return $this;
        $rcControllerBase = new ReflectionClass(ControllerBase::class);
        $routerProp = $rcControllerBase->getProperty('router');
        $routerProp->setAccessible(true);
        $routerProp->setValue($newController, $this);
        $this->controllers[] = $newController;
        return $this;
    }

    /**
     * Register a result transformer to the router.
     *
     * @param IResultTransformer $transformer
     *
     * @return static
     */
    public function registerResultTransformer( IResultTransformer $transformer ): static
    {
        if ( array_search($transformer, $this->resultTransformers, true) )
            return $this;
        $this->resultTransformers[] = $transformer;
        return $this;
    }

    /**
     * Shorthand function to write a result to the output stream after running it through the result transformers.
     *
     * @param IResult $result
     * @param bool    $close
     */
    private function writeResult( IResult $result, bool $close = false )
    {
        foreach ( $this->resultTransformers as $transformer )
            $result = $transformer->transform($result);
        $result->write($close);
    }

    /**
     * @param Throwable   $ex
     * @param string|null $message
     * @param array|null  $payload
     * @param array|null  $headers
     *
     * @return IResult
     */
    public static function CreateResultForThrowable( Throwable $ex, ?string $message = null, ?array $payload = null, ?array $headers = null ): IResult
    {
        $resp = new JsonAPIResult($ex->getCode() !== 0 ? $ex->getCode() : 500, $payload, $headers, $message ?? $ex->getMessage(), true);
        if ( $ex instanceof IRouterException )
        {
            /** @var IRouterException $ire */
            $ire = $ex;
            if ( $ire->getHttpCode() !== null )
                $resp->setHttpCode($ire->getHttpCode());
        }

        return $resp;
    }

    /**
     * Route the current request.
     *
     * @param string $basePath the web-facing path of the directory that the calling file is located.
     *
     * @return static
     */
    public function run( string $basePath = '' ): static
    {
        $basePath = rtrim($basePath, '/');
        $parsedUrl = parse_url($_SERVER['REQUEST_URI']);
        $path = '/';
        if ( isset($parsedUrl['path']) )
        {
            if ( $basePath . '/' != $parsedUrl['path'] )
            {
                $path = rtrim($parsedUrl['path'], '/');
            }
            else
            {
                $path = $parsedUrl['path'];
            }
        }

        $path = urldecode($path);

        // Get current request method
        $method = HttpVerbs::getFromString($_SERVER['REQUEST_METHOD']);
        $methodNotAllowed = false;

        try
        {
            foreach ( $this->getControllers() as $controller )
            {
                $controllerRoute = $controller->getRouteAttribute();

                if ( $controllerRoute !== null )
                {
                    //Discard controllers that don't match the first part of the route early to speed up operation.
                    if ( !preg_match(sprintf('#^%s%s#iu', $basePath, $controllerRoute->path), $path) )
                        continue;
                }

                foreach ( $controller->getAllRoutes() as $route )
                {
                    $routeRoute = $route->getRouteAttribute();
                    $routePath = ($controllerRoute?->path ?? '') . $routeRoute->path;
                    if ( $basePath != '' && $basePath != '/' )
                        $routePath = sprintf('(%s)%s', $basePath, $routePath);

                    //Check if the whole route path matches the current route
                    if ( preg_match(sprintf('#^%s$#iu', $routePath), $path, $matches) )
                    {
                        if ( $routeRoute->method & $method )
                        {

                            //Check the authentication context on the controller.
                            if ( ($auth = $controller->getAuthenticatedAttribute()) !== null )
                            {
                                if ( !($this->authenticationHandler?->checkAuthentication($controller, null, $auth) ?? true) )
                                {
                                    if ( $this->errorHandler?->handleForbidden($controller, null, $auth) ?? false )
                                        return $this;
                                    if ( !headers_sent() )
                                        header('Content-Type: ' . ContentTypes::Json);
                                    $this->writeResult(self::CreateResultForThrowable(new ClientForbiddenException()));
                                    return $this;
                                }
                            }

                            //Check the authentication context on the route.
                            if ( ($auth = $route->getAuthenticatedAttribute()) !== null )
                            {
                                if ( !($this->authenticationHandler?->checkAuthentication($controller, $route, $auth) ?? true) )
                                {
                                    if ( $this->errorHandler?->handleForbidden($controller, $route, $auth) ?? false )
                                        return $this;
                                    if ( !headers_sent() )
                                        header('Content-Type: ' . ContentTypes::Json);
                                    $this->writeResult(self::CreateResultForThrowable(new ClientForbiddenException()));
                                    return $this;
                                }
                            }

                            array_shift($matches); // Always remove first element. This contains the whole matched string.
                            if ( $basePath != '' && $basePath != '/' )
                                array_shift($matches);

                            //Execute the route.
                            $response = $route->invoke($matches, $method);
                            if ( !headers_sent() )
                                header('Content-Type: ' . $response->getContentType());

                            $this->writeResult($response);
                            return $this;
                        }
                        else
                        {
                            //Mark this method as not allowed and continue matching in case we have routes with the same name but different methods.
                            $methodNotAllowed = [ 'controller' => $controller, 'route' => $route, 'expectedMethod' => $routeRoute->method ];
                        }
                    }
                }
            }
        }
        catch ( Throwable $ex )
        {
            //If at any point we have an exception, deal with it.
            //99% of the time, the exception will come from the route invocation, instead of Gazpacho.
            if ( !($newEx = $this->errorHandler?->handle($ex) ?? false) || $newEx instanceof Throwable )
            {
                if ( !headers_sent() )
                    header('Content-Type: ' . ContentTypes::Json);
                $this->writeResult(self::CreateResultForThrowable(($newEx instanceof Throwable) ? $newEx : $ex));
            }
            return $this;
        }

        if ( $methodNotAllowed !== false )
        {
            //A route was matched, but the wrong HTTP verb was used to request it
            if ( !($this->errorHandler?->handleMethodNotAllowed($methodNotAllowed['controller'], $methodNotAllowed['route'], $methodNotAllowed['expectedMethod'], $method) ?? false) )
            {
                if ( !headers_sent() )
                    header('Content-Type: ' . ContentTypes::Json);
                $this->writeResult(self::CreateResultForThrowable(new MethodNotAllowedException()));
            }
            return $this;
        }

        //No route was matched.
        if ( !($this->errorHandler?->handleNotFound() ?? false) )
        {
            if ( !headers_sent() )
                header('Content-Type: ' . ContentTypes::Json);
            $this->writeResult(self::CreateResultForThrowable(new PageNotFoundException()));
        }
        return $this;
    }

}