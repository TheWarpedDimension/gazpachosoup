<?php

namespace WarpedDimension\GazpachoSoup\Authentication;

use Attribute;

/**
 * Indicates that the route requires authentication.
 *
 * If you wish to provide extra context to the authentication handler, extend this attribute and decorate the route/controller with that instead.
 *
 * @package WarpedDimension\GazpachoSoup
 * @author  Ned Hyett <business@warped-dimension.com>
 * @codeCoverageIgnore
 */
#[Attribute(Attribute::TARGET_CLASS | Attribute::TARGET_METHOD)]
class Authenticated
{

}