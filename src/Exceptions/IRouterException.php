<?php

namespace WarpedDimension\GazpachoSoup\Exceptions;

use Throwable;

/**
 * Implemented by exception classes that want to control router failure state.
 *
 * @package WarpedDimension\GazpachoSoup\Exceptions
 * @author  Ned Hyett <business@warped-dimension.com>
 */
interface IRouterException extends Throwable
{
    /**
     * Get the HTTP code that should be returned to the browser.
     * This solves the major complaint that some developers have with routers not responding
     * with correct HTTP codes, however is completely optional as returning null will just
     * result in a 200 with the error code in the response body.
     *
     * @return int|null
     */
    function getHttpCode(): ?int;

    /**
     * Check if this exception is intended to produce a user message instead of being a genuine fault
     * in application logic flow. For example, an "IncorrectPasswordException" should immediately
     * halt route execution and return an error to the user, but is not a defect in execution and should
     * not be logged.
     *
     * @return bool
     */
    function isUserMessage(): bool;
}