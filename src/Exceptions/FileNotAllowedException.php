<?php

namespace WarpedDimension\GazpachoSoup\Exceptions;

class FileNotAllowedException extends GazpachoSoupException
{
    const CODE = 400;
    const HTTP_CODE = 400;
    const MESSAGE = 'Uploaded file (%s) is of invalid type. Accepted types are (%s)';
}