<?php


namespace WarpedDimension\GazpachoSoup;


use JetBrains\PhpStorm\Pure;
use WarpedDimension\GazpachoSoup\Results\IResult;
use WarpedDimension\GazpachoSoup\Results\JsonAPIResult;

trait ControllerResponseTrait
{

    #[Pure]
    public static function JsonAPI(int $code, ?array $body = null, ?array $headers = null, ?string $message = null, bool $isError = false): IResult
    {
        return new JsonAPIResult($code, $body, $headers, $message, $isError);
    }

    /**
     * Create a generic success response.
     *
     * @param array|null  $body
     * @param string|null $message
     * @param array|null  $headers
     *
     * @return IResult
     */
    #[Pure]
    public static function JsonAPISuccess(?array $body = null, ?string $message = null, ?array $headers = null): IResult
    {
        return static::JsonAPI(200, $body, $headers, $message);
    }

    /**
     * Create a generic error response
     *
     * @param string     $message
     * @param array|null $body
     * @param int        $code
     *
     * @return IResult
     */
    #[Pure]
    public static function JsonAPIError(string $message, ?array $body = null, int $code = 500): IResult
    {
        return static::JsonAPI($code, $body, null, $message, true);
    }

}