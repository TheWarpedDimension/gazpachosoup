<?php

namespace WarpedDimension\GazpachoSoup\Tests\ModelParser\DefaultOverrides;

use DateTime;
use Exception;
use PHPUnit\Framework\TestCase;
use WarpedDimension\GazpachoSoup\ModelParser\DefaultOverrides\DateTimeOverride;

class DateTimeOverrideTest extends TestCase
{

    private function templateFuncBlank( DateTime $dt )
    {
        //template function to get a nicely generated parameter for testing.
    }

    private function templateFuncUTC( #[DateTimeOverride("UTC")] DateTime $dt )
    {
        //template function to get a nicely generated parameter for testing.
    }

    private function templateFuncCEST( #[DateTimeOverride("Europe/Berlin")] DateTime $dt )
    {
        //template function to get a nicely generated parameter for testing.
    }

    public function testNoOverrideISO8601()
    {
        $rc = new \ReflectionClass(static::class);
        $param = $rc->getMethod('templateFuncBlank')->getParameters()[0];
        $dateText = "2021-07-21 08:46:32";
        /** @var DateTime $result */
        $result = DateTimeOverride::convertDateTime($param, $dateText);
        $this->assertEquals(1626857192, $result->getTimestamp());
    }

    public function testNoOverrideEpoch()
    {
        $rc = new \ReflectionClass(static::class);
        $param = $rc->getMethod('templateFuncBlank')->getParameters()[0];
        $dateText = "1626857192";
        /** @var DateTime $result */
        $result = DateTimeOverride::convertDateTime($param, $dateText);
        $this->assertEquals(1626857192, $result->getTimestamp());
    }

    public function testUTCISO8601()
    {
        $rc = new \ReflectionClass(static::class);
        $param = $rc->getMethod('templateFuncUTC')->getParameters()[0];
        $dateText = "2021-07-21 08:46:32";
        /** @var DateTime $result */
        $result = DateTimeOverride::convertDateTime($param, $dateText);
        $this->assertEquals(1626857192, $result->getTimestamp());
        $this->assertEquals('UTC', $result->getTimezone()->getName());
    }

    public function testUTCEpoch()
    {
        $rc = new \ReflectionClass(static::class);
        $param = $rc->getMethod('templateFuncUTC')->getParameters()[0];
        $dateText = "1626857192";
        /** @var DateTime $result */
        $result = DateTimeOverride::convertDateTime($param, $dateText);
        $this->assertEquals(1626857192, $result->getTimestamp());
        $this->assertEquals('+00:00', $result->getTimezone()->getName());
    }

    public function testBadDateFormat()
    {
        $rc = new \ReflectionClass(static::class);
        $param = $rc->getMethod('templateFuncBlank')->getParameters()[0];
        $dateText = "2021-baddata07-21 08:46:32";
        /** @var DateTime $result */
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('Invalid DateTime string.');
        DateTimeOverride::convertDateTime($param, $dateText);
    }

}
