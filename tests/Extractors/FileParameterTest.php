<?php

namespace WarpedDimension\GazpachoSoup\Tests\Extractors;

use PHPUnit\Framework\TestCase;
use WarpedDimension\GazpachoSoup\Exceptions\FileNotAllowedException;
use WarpedDimension\GazpachoSoup\Extractors\FileParameter;

class FileParameterTest extends TestCase
{

    private const varName = 'testvar';
    private const varValue = [ 'value' => 'bad', 'type' => 'text/html' ];
    private const badVarName = 'bad';
    private const badVarValue = [ 'value' => 'bad', 'type' => 'unknown' ];

    protected function tearDown(): void
    {
        foreach ( $_FILES as $k => $v )
        {
            unset($_FILES[ $k ]);
        }
    }

    public function testIsSet()
    {
        $qp = new FileParameter();
        $this->assertFalse($qp->isSet(self::varName), sprintf("FileParameter thinks that %s is set when it isn't.", self::varName));
        $_FILES[ self::varName ] = self::varValue;
        $this->assertTrue($qp->isSet(self::varName), sprintf("FileParameter thinks that %s is not set when it is.", self::varName));
    }

    public function testIsSetWithOverride()
    {
        $qp = new FileParameter(self::varName);
        $_FILES[ self::badVarName ] = self::badVarValue;
        $this->assertFalse($qp->isSet(self::badVarName), sprintf("FileParameter thinks that %s is set when it isn't.", self::varName));
        $_FILES[ self::varName ] = self::varValue;
        unset($_FILES[ self::badVarName ]);
        $this->assertTrue($qp->isSet(self::badVarName), sprintf("FileParameter thinks that %s is not set when it is.", self::varName));
    }

    public function testGetParameterValue()
    {
        $qp = new FileParameter();
        $this->assertNull($qp->getParameterValue(self::varName), sprintf("FileParameter thinks that %s is set when it isn't.", self::varName));
        $_FILES[ self::varName ] = self::varValue;
        $this->assertEquals(self::varValue, $qp->getParameterValue(self::varName), "FileParameter got invalid value.");
    }

    public function testGetParameterValueWithOverride()
    {
        $qp = new FileParameter(self::varName);
        $_FILES[ self::badVarName ] = self::badVarValue;
        $this->assertNull($qp->getParameterValue(self::badVarName), sprintf("FileParameter thinks that %s is set when it isn't.", self::varName));
        $_FILES[ self::varName ] = self::varValue;
        $this->assertEquals(self::varValue, $qp->getParameterValue(self::badVarName), "FileParameter got invalid value.");
    }

    public function testMimeTypeCheck()
    {
        $qp = new FileParameter(self::varName, [ 'application/json' ]);
        $_FILES[ self::varName ] = self::varValue;
        $this->expectException(FileNotAllowedException::class);
        $qp->getParameterValue(self::varName);
    }

}
