<?php

namespace WarpedDimension\GazpachoSoup\Tests\Extractors;

use PHPUnit\Framework\TestCase;
use WarpedDimension\GazpachoSoup\Router;
use WarpedDimension\GazpachoSoup\Tests\Framework\RawController;

class RawParameterTest extends TestCase
{

    public function testGetRawParameterValue()
    {
        $_SERVER['REQUEST_URI'] = '/raw/fetch_raw_param';
        $_SERVER['REQUEST_METHOD'] = 'POST';
        $router = new Router([]);
        $router->registerController(new RawController());
        $router->run();
    }

    public function testGetRawParameterValueDoesNotExist()
    {
        $_SERVER['REQUEST_URI'] = '/raw/fetch_raw_param/no_exist';
        $_SERVER['REQUEST_METHOD'] = 'POST';
        $router = new Router([]);
        $router->registerController(new RawController());
        ob_start();
        $router->run();
        $data = json_decode(ob_get_clean(), true);
        $this->assertEquals(400, $data['code']);
        $this->assertEquals("Missing required parameter (body_item_3) in raw post or is wrong type.", $data['message']);
    }

}
