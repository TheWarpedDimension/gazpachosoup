<?php

namespace WarpedDimension\GazpachoSoup\Tests\Extractors;

use PHPUnit\Framework\TestCase;
use WarpedDimension\GazpachoSoup\Extractors\QueryParameter;

class QueryParameterTest extends TestCase
{

    private const getVarName = 'testvar';
    private const getVarValue = 'thisisatest';
    private const badGetVarName = 'bad';
    private const badGetVarValue = 'reallybad';

    protected function tearDown(): void
    {
        foreach ( $_GET as $k => $v )
        {
            unset($_GET[ $k ]);
        }
    }

    public function testIsSet()
    {
        $qp = new QueryParameter();
        $this->assertFalse($qp->isSet(self::getVarName), sprintf("QueryParameter thinks that %s is set when it isn't.", self::getVarName));
        $_GET[ self::getVarName ] = self::getVarValue;
        $this->assertTrue($qp->isSet(self::getVarName), sprintf("QueryParameter thinks that %s is not set when it is.", self::getVarName));
    }

    public function testIsSetWithOverride()
    {
        $qp = new QueryParameter(self::getVarName);
        $_GET[ self::badGetVarName ] = self::badGetVarValue;
        $this->assertFalse($qp->isSet(self::badGetVarName), sprintf("QueryParameter thinks that %s is set when it isn't.", self::getVarName));
        $_GET[ self::getVarName ] = self::getVarValue;
        unset($_GET[ self::badGetVarName ]);
        $this->assertTrue($qp->isSet(self::badGetVarName), sprintf("QueryParameter thinks that %s is not set when it is.", self::getVarName));
    }

    public function testGetParameterValue()
    {
        $qp = new QueryParameter();
        $this->assertNull($qp->getParameterValue(self::getVarName), sprintf("QueryParameter thinks that %s is set when it isn't.", self::getVarName));
        $_GET[ self::getVarName ] = self::getVarValue;
        $this->assertEquals(self::getVarValue, $qp->getParameterValue(self::getVarName), "QueryParameter got invalid value.");
    }

    public function testGetParameterValueWithOverride()
    {
        $qp = new QueryParameter(self::getVarName);
        $_GET[ self::badGetVarName ] = self::badGetVarValue;
        $this->assertNull($qp->getParameterValue(self::badGetVarName), sprintf("QueryParameter thinks that %s is set when it isn't.", self::getVarName));
        $_GET[ self::getVarName ] = self::getVarValue;
        $this->assertEquals(self::getVarValue, $qp->getParameterValue(self::badGetVarName), "QueryParameter got invalid value.");
    }

}
