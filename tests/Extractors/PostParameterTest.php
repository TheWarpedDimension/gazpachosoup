<?php

namespace WarpedDimension\GazpachoSoup\Tests\Extractors;

use PHPUnit\Framework\TestCase;
use WarpedDimension\GazpachoSoup\Extractors\PostParameter;

class PostParameterTest extends TestCase
{

    private const postVarName = 'testvar';
    private const postVarValue = 'thisisatest';
    private const badPostVarName = 'bad';
    private const badPostVarValue = 'reallybad';

    protected function tearDown(): void
    {
        foreach ( $_POST as $k => $v )
        {
            unset($_POST[ $k ]);
        }
    }

    public function testIsSet()
    {
        $qp = new PostParameter();
        $this->assertFalse($qp->isSet(self::postVarName), sprintf("PostParameter thinks that %s is set when it isn't.", self::postVarName));
        $_POST[ self::postVarName ] = self::postVarValue;
        $this->assertTrue($qp->isSet(self::postVarName), sprintf("PostParameter thinks that %s is not set when it is.", self::postVarName));
    }

    public function testIsSetWithOverride()
    {
        $qp = new PostParameter(self::postVarName);
        $_POST[ self::badPostVarName ] = self::badPostVarValue;
        $this->assertFalse($qp->isSet(self::badPostVarName), sprintf("PostParameter thinks that %s is set when it isn't.", self::postVarName));
        $_POST[ self::postVarName ] = self::postVarValue;
        unset($_POST[ self::badPostVarName ]);
        $this->assertTrue($qp->isSet(self::badPostVarName), sprintf("PostParameter thinks that %s is not set when it is.", self::postVarName));
    }

    public function testGetParameterValue()
    {
        $qp = new PostParameter();
        $this->assertNull($qp->getParameterValue(self::postVarName), sprintf("PostParameter thinks that %s is set when it isn't.", self::postVarName));
        $_POST[ self::postVarName ] = self::postVarValue;
        $this->assertEquals(self::postVarValue, $qp->getParameterValue(self::postVarName), "PostParameter got invalid value.");
    }

    public function testGetParameterValueWithOverride()
    {
        $qp = new PostParameter(self::postVarName);
        $_POST[ self::badPostVarName ] = self::badPostVarValue;
        $this->assertNull($qp->getParameterValue(self::badPostVarName), sprintf("PostParameter thinks that %s is set when it isn't.", self::postVarName));
        $_POST[ self::postVarName ] = self::postVarValue;
        $this->assertEquals(self::postVarValue, $qp->getParameterValue(self::badPostVarName), "PostParameter got invalid value.");
    }

}
