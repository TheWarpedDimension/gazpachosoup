<?php

namespace WarpedDimension\GazpachoSoup\Tests\Finders;

use PHPUnit\Framework\TestCase;
use WarpedDimension\GazpachoSoup\Finders\ManifestControllerFinder;

class ManifestControllerFinderTest extends TestCase
{

    public function testFindControllers()
    {
        $finder = new ManifestControllerFinder(__DIR__ . '/../Framework');
        $controllers = $finder->getControllerNamespaces();
        $this->assertNotCount(0, $controllers, 'No controllers were found.');
        $this->assertTrue(class_exists($controllers[0]), 'Found controller does not exist.');
    }

}
