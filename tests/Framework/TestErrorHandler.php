<?php

namespace WarpedDimension\GazpachoSoup\Tests\Framework;

use Throwable;
use WarpedDimension\GazpachoSoup\Authentication\Authenticated;
use WarpedDimension\GazpachoSoup\ControllerBase;
use WarpedDimension\GazpachoSoup\IErrorHandler;
use WarpedDimension\GazpachoSoup\RouteWrapper;

class TestErrorHandler implements IErrorHandler
{

    private bool $generic = false;
    private bool $forbidden = false;
    private bool $methodNotAllowed = false;
    private bool $notFound = false;

    public function reset()
    {
        $this->generic = false;
        $this->forbidden = false;
        $this->methodNotAllowed = false;
        $this->notFound = false;
    }

    /**
     * @inheritDoc
     */
    function handle( Throwable $throwable ): Throwable|bool
    {
        $this->generic = true;
        return false;
    }

    /**
     * @inheritDoc
     */
    function handleForbidden( ControllerBase $controller, ?RouteWrapper $route, Authenticated $authenticationContext ): bool
    {
        $this->forbidden = true;
        return false;
    }

    /**
     * @inheritDoc
     */
    function handleMethodNotAllowed( ControllerBase $controller, ?RouteWrapper $route, int $method, int $expectedMethod ): bool
    {
        $this->methodNotAllowed = true;
        return false;
    }

    /**
     * @inheritDoc
     */
    function handleNotFound(): bool
    {
        $this->notFound = true;
        return false;
    }

    /**
     * @return bool
     */
    public function isForbidden(): bool
    {
        return $this->forbidden;
    }

    /**
     * @return bool
     */
    public function isMethodNotAllowed(): bool
    {
        return $this->methodNotAllowed;
    }

    /**
     * @return bool
     */
    public function isNotFound(): bool
    {
        return $this->notFound;
    }

    /**
     * @return bool
     */
    public function isGeneric(): bool
    {
        return $this->generic;
    }
}