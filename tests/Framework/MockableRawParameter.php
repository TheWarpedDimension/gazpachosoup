<?php

namespace WarpedDimension\GazpachoSoup\Tests\Framework;

use Attribute;
use WarpedDimension\GazpachoSoup\Extractors\RawParameter;

#[Attribute(Attribute::TARGET_PARAMETER)]
class MockableRawParameter extends RawParameter
{

    protected static function getRequestBodyData(): string
    {
        return json_encode([
            "body_item_1" => "body_item_1_value",
            "body_item_2" => "body_item_2_value"
        ]);
    }

}